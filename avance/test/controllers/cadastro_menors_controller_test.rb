require 'test_helper'

class CadastroMenorsControllerTest < ActionController::TestCase
  setup do
    @cadastro_menor = cadastro_menors(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:cadastro_menors)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create cadastro_menor" do
    assert_difference('CadastroMenor.count') do
      post :create, cadastro_menor: { Agente_Causador: @cadastro_menor.Agente_Causador, Brasilia_DF: @cadastro_menor.Brasilia_DF, CPF: @cadastro_menor.CPF, Cidade: @cadastro_menor.Cidade, Data_Acidente: @cadastro_menor.Data_Acidente, Data_de_Nascimento: @cadastro_menor.Data_de_Nascimento, Descrição_acidente: @cadastro_menor.Descrição_acidente, Endereço_atual: @cadastro_menor.Endereço_atual, Estado_Civil: @cadastro_menor.Estado_Civil, Filiação: @cadastro_menor.Filiação, Idade: @cadastro_menor.Idade, Instrução: @cadastro_menor.Instrução, Moradia: @cadastro_menor.Moradia, Naturalidade: @cadastro_menor.Naturalidade, Nome: @cadastro_menor.Nome, Nome_Responsavel: @cadastro_menor.Nome_Responsavel, Profissão: @cadastro_menor.Profissão, Queimadura: @cadastro_menor.Queimadura, RG: @cadastro_menor.RG, Região_Atingida: @cadastro_menor.Região_Atingida, Renda_Familiar: @cadastro_menor.Renda_Familiar, Sequela: @cadastro_menor.Sequela, Sexo: @cadastro_menor.Sexo, Telone: @cadastro_menor.Telone }
    end

    assert_redirected_to cadastro_menor_path(assigns(:cadastro_menor))
  end

  test "should show cadastro_menor" do
    get :show, id: @cadastro_menor
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @cadastro_menor
    assert_response :success
  end

  test "should update cadastro_menor" do
    patch :update, id: @cadastro_menor, cadastro_menor: { Agente_Causador: @cadastro_menor.Agente_Causador, Brasilia_DF: @cadastro_menor.Brasilia_DF, CPF: @cadastro_menor.CPF, Cidade: @cadastro_menor.Cidade, Data_Acidente: @cadastro_menor.Data_Acidente, Data_de_Nascimento: @cadastro_menor.Data_de_Nascimento, Descrição_acidente: @cadastro_menor.Descrição_acidente, Endereço_atual: @cadastro_menor.Endereço_atual, Estado_Civil: @cadastro_menor.Estado_Civil, Filiação: @cadastro_menor.Filiação, Idade: @cadastro_menor.Idade, Instrução: @cadastro_menor.Instrução, Moradia: @cadastro_menor.Moradia, Naturalidade: @cadastro_menor.Naturalidade, Nome: @cadastro_menor.Nome, Nome_Responsavel: @cadastro_menor.Nome_Responsavel, Profissão: @cadastro_menor.Profissão, Queimadura: @cadastro_menor.Queimadura, RG: @cadastro_menor.RG, Região_Atingida: @cadastro_menor.Região_Atingida, Renda_Familiar: @cadastro_menor.Renda_Familiar, Sequela: @cadastro_menor.Sequela, Sexo: @cadastro_menor.Sexo, Telone: @cadastro_menor.Telone }
    assert_redirected_to cadastro_menor_path(assigns(:cadastro_menor))
  end

  test "should destroy cadastro_menor" do
    assert_difference('CadastroMenor.count', -1) do
      delete :destroy, id: @cadastro_menor
    end

    assert_redirected_to cadastro_menors_path
  end
end

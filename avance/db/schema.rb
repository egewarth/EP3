# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151208202112) do

  create_table "cadastro_menors", force: :cascade do |t|
    t.string   "Nome"
    t.date     "Data_de_Nascimento"
    t.boolean  "Sexo"
    t.string   "Idade"
    t.string   "Naturalidade"
    t.string   "Filiação"
    t.string   "Nome_Responsavel"
    t.string   "RG"
    t.string   "CPF"
    t.string   "Estado_Civil"
    t.string   "Profissão"
    t.string   "Instrução"
    t.string   "Endereço_atual"
    t.string   "Cidade"
    t.string   "Telone"
    t.string   "Moradia"
    t.string   "Renda_Familiar"
    t.text     "Descrição_acidente"
    t.string   "Agente_Causador"
    t.boolean  "Queimadura"
    t.string   "Região_Atingida"
    t.boolean  "Sequela"
    t.date     "Data_Acidente"
    t.date     "Brasilia_DF"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "usuarios", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "usuarios", ["email"], name: "index_usuarios_on_email", unique: true
  add_index "usuarios", ["reset_password_token"], name: "index_usuarios_on_reset_password_token", unique: true

end

class CreateCadastroMenors < ActiveRecord::Migration
  def change
    create_table :cadastro_menors do |t|
      t.string :Nome
      t.date :Data_de_Nascimento
      t.boolean :Sexo
      t.string :Idade
      t.string :Naturalidade
      t.string :Filiação
      t.string :Nome_Responsavel
      t.string :RG
      t.string :CPF
      t.string :Estado_Civil
      t.string :Profissão
      t.string :Instrução
      t.string :Endereço_atual
      t.string :Cidade
      t.string :Telone
      t.string :Moradia
      t.string :Renda_Familiar
      t.text :Descrição_acidente
      t.string :Agente_Causador
      t.boolean :Queimadura
      t.string :Região_Atingida
      t.boolean :Sequela
      t.date :Data_Acidente
      t.date :Brasilia_DF

      t.timestamps null: false
    end
  end
end

json.array!(@cadastro_menors) do |cadastro_menor|
  json.extract! cadastro_menor, :id, :Nome, :Data_de_Nascimento, :Sexo, :Idade, :Naturalidade, :Filiação, :Nome_Responsavel, :RG, :CPF, :Estado_Civil, :Profissão, :Instrução, :Endereço_atual, :Cidade, :Telone, :Moradia, :Renda_Familiar, :Descrição_acidente, :Agente_Causador, :Queimadura, :Região_Atingida, :Sequela, :Data_Acidente, :Brasilia_DF
  json.url cadastro_menor_url(cadastro_menor, format: :json)
end

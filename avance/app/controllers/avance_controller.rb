class AvanceController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource

  def home
  end

  def donates
  end

  def login
  end
end

class CadastroMenorsController < ApplicationController

  before_action :set_cadastro_menor, only: [:show, :edit, :update, :destroy]

  # GET /cadastro_menors
  # GET /cadastro_menors.json
  def index
    @cadastro_menors = CadastroMenor.all
  end

  # GET /cadastro_menors/1
  # GET /cadastro_menors/1.json
  def show
  end

  # GET /cadastro_menors/new
  def new
    @cadastro_menor = CadastroMenor.new
  end

  # GET /cadastro_menors/1/edit
  def edit
  end

  # POST /cadastro_menors
  # POST /cadastro_menors.json
  def create
    @cadastro_menor = CadastroMenor.new(cadastro_menor_params)

    respond_to do |format|
      if @cadastro_menor.save
        format.html { redirect_to @cadastro_menor, notice: 'Cadastro foi criado com sucesso.' }
        format.json { render :show, status: :created, location: @cadastro_menor }
      else
        format.html { render :new }
        format.json { render json: @cadastro_menor.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cadastro_menors/1
  # PATCH/PUT /cadastro_menors/1.json
  def update
    respond_to do |format|
      if @cadastro_menor.update(cadastro_menor_params)
        format.html { redirect_to @cadastro_menor, notice: 'Cadastro foi atualizado com sucesso.' }
        format.json { render :show, status: :ok, location: @cadastro_menor }
      else
        format.html { render :edit }
        format.json { render json: @cadastro_menor.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cadastro_menors/1
  # DELETE /cadastro_menors/1.json
  def destroy
    @cadastro_menor.destroy
    respond_to do |format|
      format.html { redirect_to cadastro_menors_url, notice: 'Cadastro foi deletado com sucesso.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cadastro_menor
      @cadastro_menor = CadastroMenor.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cadastro_menor_params
      params.require(:cadastro_menor).permit(:Nome, :Data_de_Nascimento, :Sexo, :Idade, :Naturalidade, :Filiação, :Nome_Responsavel, :RG, :CPF, :Estado_Civil, :Profissão, :Instrução, :Endereço_atual, :Cidade, :Telone, :Moradia, :Renda_Familiar, :Descrição_acidente, :Agente_Causador, :Queimadura, :Região_Atingida, :Sequela, :Data_Acidente, :Brasilia_DF)
    end
end
